﻿// 1512484.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512484.h"
#include <windowsx.h>
#include <vector>
#include <fstream>
#include <locale>
#include <codecvt>
#include <string>
#include <commctrl.h>
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
using namespace Gdiplus;
//using namespace std;
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
HWND hComboBox, hMoney, hContent, hSumMoney, hAdd;
HWND g_hWnd;
HWND g_hListview;
HWND hTemp;
HWND hClose;
typedef struct Item
{
	WCHAR Type[20];
	unsigned long long Money;
	std::wstring Content;
};
std::vector<Item> listItem;
int ItemCount = 0;
long long SumMoney = 0;
WCHAR types[6][25] ={L"Ăn uống", L"Di chuyển", L"Nhà cửa", L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ",};
unsigned long long Ratio[6];
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void setWindowText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter);
void writeToFile(std::wstring path);
void loadFromFile(std::wstring path);
void loadToListView(HWND m_hListview);
void GetRatio(unsigned long long A[]);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512484, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512484));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512484);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"Quản lý chi tiêu", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 600, 700, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);
		// Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		//GroupBox1
		hComboBox = CreateWindowEx(0, WC_COMBOBOX, TEXT(""),
			CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
			40, 60, 110, 100, hWnd, NULL, hInst,
			NULL);
		SendMessage(hComboBox, WM_SETFONT, WPARAM(hFont), TRUE);

		hContent = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_MULTILINE | ES_AUTOVSCROLL,
			170, 60, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hContent, WM_SETFONT, WPARAM(hFont), TRUE);

		hMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER,
			300, 60, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hMoney, WM_SETFONT, WPARAM(hFont), TRUE);

		hAdd = CreateWindowEx(0, L"BUTTON", L"THÊM", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 
			440, 53, 90, 35, hWnd, (HMENU)IDC_ADD, hInst, NULL);
		SendMessage(hAdd, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE,
			40, 40, 65, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Nội dung:", WS_CHILD | WS_VISIBLE, 
			170, 40, 50, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Số tiền:", WS_CHILD | WS_VISIBLE, 
			300, 40, 40, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		HWND hGroupboxA = CreateWindowEx(0, L"BUTTON", L"Thêm một loại chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 
			20, 10, 540, 120, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxA, WM_SETFONT, WPARAM(hFont), TRUE);

		//Initialize combobox
		TCHAR A[16];
		int  k = 0;
		memset(&A, 0, sizeof(A));
		for (k = 0; k < 6; k++)
		{
			wcscpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)types[k]);
			Ratio[k] = 0;
			//Insert to combobox
			SendMessage(hComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
		}
		// Set ComboBox default 
		SendMessage(hComboBox, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);


		//Groupbox2
		HWND hGroupboxB = CreateWindowEx(0, L"BUTTON", L"Toàn bộ danh sách các chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 
			20, 150, 540, 200, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxB, WM_SETFONT, WPARAM(hFont), TRUE);

		//Init listview properties
		long extStyle = WS_EX_CLIENTEDGE;
		long style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

		//Create ListView
		g_hListview = CreateWindowEx(extStyle, WC_LISTVIEW, _T("List View"),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | style,
			40, 180, 500, 150, hWnd, (HMENU)IDL_LISTVIEW, hInst, NULL);

		//Init 3 columns
		LVCOLUMN lvCol;

		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

		//Insert type, money and description column
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 125;
		lvCol.pszText = _T("Loại chi tiêu");
		ListView_InsertColumn(g_hListview, 0, &lvCol);

		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 130;
		lvCol.pszText = _T("Nội dung");
		ListView_InsertColumn(g_hListview, 1, &lvCol);

		lvCol.fmt = LVCFMT_LEFT;
		lvCol.pszText = _T("Số tiền");
		lvCol.cx = 100;
		ListView_InsertColumn(g_hListview, 2, &lvCol);


		//Groupbox3
		HWND hGroupboxC = CreateWindowEx(0, L"BUTTON", L"Thông tin thống kê", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 
			20, 370, 540, 205, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxC, WM_SETFONT, WPARAM(hFont), TRUE);

		hSumMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY, 
			250, 400, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hSumMoney, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Tổng cộng", WS_CHILD | WS_VISIBLE, 175, 403, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE, 90, 507, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE, 90, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE, 260, 507, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE, 260, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE, 420, 507, 90, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE, 420, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hAdd = CreateWindowEx(0, L"BUTTON", L"THOÁT", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			240, 590, 90, 35, hWnd, (HMENU)IDC_CLOSE, hInst, NULL);
		SendMessage(hAdd, WM_SETFONT, WPARAM(hFont), TRUE);

		loadFromFile(L"test.txt");
		loadToListView(g_hListview);
		ItemCount = listItem.size();

		setWindowText(hSumMoney, SumMoney, L"", L"");
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case IDC_ADD:
			{	
				Item item;
				WCHAR* buffer;

				//Get content
				int len = GetWindowTextLength(hContent);
				if (len > 0)
				{
					buffer = new WCHAR[len + 1];
					GetWindowText(hContent, buffer, len + 1);
					item.Content = std::wstring(buffer);
				}
				else
				{
					MessageBox(g_hWnd, L"Bạn chưa điền vào nội dung!!", L"Thông báo", MB_ICONWARNING | MB_OK);
					break;
				}

				//Get money
				len = GetWindowTextLength(hMoney);
				if (len > 0)
				{
					buffer = new WCHAR[len + 1];
					GetWindowText(hMoney, buffer, len + 1);
					item.Money = _wtoi64(buffer);
				}
				else
				{
					MessageBox(g_hWnd, L"Bạn chưa điền vào số tiền!!", L"Thông báo", MB_ICONWARNING | MB_OK);
					break;
				}

				//Get type
				buffer = new WCHAR[20];
				GetWindowText(hComboBox, buffer, 20);
				wcscpy_s(item.Type, buffer);

				//Insert to list Item
				listItem.push_back(item);

				if (wcscmp(item.Type, L"Ăn uống") == 0)
					Ratio[0] += item.Money;
				if (wcscmp(item.Type, L"Di chuyển") == 0)
					Ratio[1] += item.Money;
				if (wcscmp(item.Type, L"Nhà cửa") == 0)
					Ratio[2] += item.Money;
				if (wcscmp(item.Type, L"Xe cộ") == 0)
					Ratio[3] += item.Money;
				if (wcscmp(item.Type, L"Nhu yếu phẩm") == 0)
					Ratio[4] += item.Money;
				if (wcscmp(item.Type, L"Dịch vụ") == 0)
					Ratio[5] += item.Money;

				//Add to sum money
				SumMoney += item.Money;
				
				//Add to ListView
				LV_ITEM lv;

				lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;

				//Insert type to first column
				lv.iItem = ItemCount;
				lv.iSubItem = 0;
				lv.pszText = item.Type;

				ListView_InsertItem(g_hListview, &lv);
				lv.mask = LVIF_TEXT;

				//Load content to second column
				lv.iSubItem = 1;
				lv.pszText = (WCHAR*)item.Content.c_str();
				ListView_SetItem(g_hListview, &lv);

				//Load money to third column
				lv.iSubItem = 2;
				buffer = new WCHAR[20];
				wsprintf(buffer, L"%I64d", item.Money);
				lv.pszText = buffer;
				ListView_SetItem(g_hListview, &lv);

				setWindowText(hSumMoney, SumMoney, L"", L"");

				//Redraw the window
				RedrawWindow(g_hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
				RedrawWindow(hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
				break;
			}
			case IDC_CLOSE:
				writeToFile(L"test.txt");
				DestroyWindow(hWnd);
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
				writeToFile(L"test.txt");
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
			Pen* pen = new Pen(Color(255, 0, 0, 0), 1);
			Graphics* graphics = new Graphics(hdc);
			HatchBrush* Red = new HatchBrush(HatchStyleCross, Color(255, 255, 0, 0), Color(255, 255, 0, 0));
			HatchBrush* Orange = new HatchBrush(HatchStyleCross, Color(255, 255, 100, 0), Color(255, 255, 100, 0));
			HatchBrush* Yellow = new HatchBrush(HatchStyleCross, Color(255, 255, 255, 0), Color(255, 255, 255, 0));
			HatchBrush* Green = new HatchBrush(HatchStyleCross, Color(255, 0, 128, 0), Color(255, 0, 128, 0));
			HatchBrush* Blue = new HatchBrush(HatchStyleCross, Color(255, 0, 0, 160), Color(255, 0, 0, 160));
			HatchBrush* Purple = new HatchBrush(HatchStyleCross, Color(255, 128, 0, 128), Color(255, 128, 0, 128));
			
			int pos = 40;
			graphics->DrawRectangle(pen, pos, 443, 497, 35);
			if (SumMoney != 0)
				for (int i = 0; i < 6; i++)
				{
					graphics->DrawRectangle(pen, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 0)
						graphics->FillRectangle(Green, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 1)
						graphics->FillRectangle(Blue, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 2)
						graphics->FillRectangle(Red, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 3)
						graphics->FillRectangle(Purple, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 4)
						graphics->FillRectangle(Orange, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					if (i == 5)
						graphics->FillRectangle(Yellow, pos, 443, 500 * Ratio[i] / SumMoney, 35);
					pos += 500 * Ratio[i] / SumMoney;
				}

			graphics->DrawRectangle(pen, 40, 510, 30, 7);
			graphics->FillRectangle(Green, 40, 510, 30, 7);

			graphics->DrawRectangle(pen, 40, 540, 30, 7);
			graphics->FillRectangle(Blue, 40, 540, 30, 7);

			graphics->DrawRectangle(pen, 210, 510, 30, 7);
			graphics->FillRectangle(Red, 210, 510, 30, 7);

			graphics->DrawRectangle(pen, 210, 540, 30, 7);
			graphics->FillRectangle(Purple, 210, 540, 30, 7);

			graphics->DrawRectangle(pen, 370, 510, 30, 7);
			graphics->FillRectangle(Orange, 370, 510, 30, 7);

			graphics->DrawRectangle(pen, 370, 540, 30, 7);
			graphics->FillRectangle(Yellow, 370, 540, 30, 7);

			delete pen;
			delete graphics;
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		writeToFile(L"test.txt");
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void setWindowText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter)
{
	//Create the string
	WCHAR buffer[255];
	if (value < 10)
		wsprintf(buffer, L"0%I64d", value);
	else
		wsprintf(buffer, L"%I64d", value);

	//Set text
	SetWindowText(hWnd, (textBefore + std::wstring(buffer) + textAfter).c_str());
}


void writeToFile(std::wstring path)
{
	//Open file
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wofstream f(path);
	f.imbue(utf8_locale);

	f << SumMoney << std::endl;
	for (int i = 0; i < listItem.size(); i++)
	{
		f << std::wstring(listItem[i].Type) << std::endl;
		f << std::wstring(listItem[i].Content) << std::endl;
		f << listItem[i].Money << std::endl;
	}
	//Close file
	f.close();
}

void loadFromFile(std::wstring path)
{
	//Open file
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wfstream f;
	f.imbue(utf8_locale);
	f.open(path, std::ios::in);

	std::wstring buffer;
	if (f.is_open())
	{
		//Get sum money
		if (getline(f, buffer))
			SumMoney = _wtoi64(buffer.c_str());

		//Get items
		while (getline(f, buffer))
		{
			Item item;

			wcscpy_s(item.Type, buffer.c_str());
			getline(f, buffer);
			item.Content = buffer;
			getline(f, buffer);
			item.Money = _wtoi64(buffer.c_str());

			listItem.push_back(item);
		}
	}
	//Close file
	f.close();
}

void loadToListView(HWND m_hListview)
{
	LV_ITEM lv;
	WCHAR* buffer = new WCHAR[20];

	for (int i = 0; i < listItem.size(); i++)
	{
		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;

		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = listItem[i].Type;

		ListView_InsertItem(m_hListview, &lv);

		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = (WCHAR*)listItem[i].Content.c_str();
		ListView_SetItem(m_hListview, &lv);
		lv.iSubItem = 2;
		buffer = new WCHAR[20];
		wsprintf(buffer, L"%I64d", listItem[i].Money);
		lv.pszText = buffer;
		ListView_SetItem(m_hListview, &lv);
		if (wcscmp(listItem[i].Type, L"Ăn uống") == 0)
			Ratio[0] += listItem[i].Money;
		if (wcscmp(listItem[i].Type, L"Di chuyển") == 0)
			Ratio[1] += listItem[i].Money;
		if (wcscmp(listItem[i].Type, L"Nhà cửa") == 0)
			Ratio[2] += listItem[i].Money;
		if (wcscmp(listItem[i].Type, L"Xe cộ") == 0)
			Ratio[3] += listItem[i].Money;
		if (wcscmp(listItem[i].Type, L"Nhu yếu phẩm") == 0)
			Ratio[4] += listItem[i].Money;
		if (wcscmp(listItem[i].Type, L"Dịch vụ") == 0)
			Ratio[5] += listItem[i].Money;
	}
}