//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512484_QuickLaunch.rc
//
#define IDC_MYICON                      2
#define IDD_MY1512484QUICKLAUNCH_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY1512484QUICKLAUNCH        107
#define IDI_SMALL                       108
#define IDC_MY1512484QUICKLAUNCH        109
#define IDC_GROUPBOX                    113
#define IDL_LISTVIEW                    114
#define IDR_MAINFRAME                   128
#define IDD_STATISTIC                   129
#define IDI_ICON1                       130
#define IDC_BTN_STATISTIC               1007
#define TRAY_ICON_NOTIFICATION          1008
#define IDC_TRAY_ICON_SCAN              1009
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           115
#endif
#endif
