// 1512484_QuickLaunch.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512484_QuickLaunch.h"
#include <windowsX.h>
#include <commctrl.h>
#include <string>
#include <cstdlib>
#include <shellapi.h>
#include <fstream>
#include <sstream>
#include <locale>
#include <codecvt>
#include <iostream>
#include <algorithm>
#include <vector>
#pragma comment(lib, "shlwapi.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
#define MAX_LOADSTRING 100
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <shellapi.h>
#include <msi.h>
#include <Shlobj.h>
#pragma comment(lib, "Msi.lib")
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
NOTIFYICONDATA nid;
HMENU hPopMenu;
HWND g_hSearchBox;
HWND g_hListview;
typedef struct Program
{
	int pr_index;
	std::wstring pr_Name;
	std::wstring pr_Icon;
	std::wstring pr_Location;
	int pr_freq = 0;
};
std::vector<Program> g_List;
std::vector<Program> g_SearchList;
std::vector<std::wstring> g_FreqNameList;
std::vector<int> g_FreqNumbList;
WCHAR keyword[255];
int g_Sum = 0;
bool scan = false;
bool change = false;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK StatisticDialog(HWND, UINT, WPARAM, LPARAM);
void ScanProgram();
void LoadToListView(std::vector<Program> list);
void WriteToFile();
void ReadFromFile();
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1512484QUICKLAUNCH, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512484QUICKLAUNCH));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY1512484QUICKLAUNCH);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"MY QUICKLAUNCH", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 600, 460, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
   HICON hMainIcon;
   hMainIcon = LoadIcon(hInstance, (LPCTSTR)MAKEINTRESOURCE(IDI_ICON1));

   nid.cbSize = sizeof(NOTIFYICONDATA); // sizeof the struct in bytes 
   nid.hWnd = (HWND)hWnd;              //handle of the window which will process this app. messages 
   nid.uID = IDI_ICON1;           //ID of the icon that will appear in the system tray 
   nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP; //ORing of all the flags 
   nid.hIcon = hMainIcon; // handle of the Icon to be displayed, obtained from LoadIcon 
   nid.uCallbackMessage = TRAY_ICON_NOTIFICATION;
   LoadString(hInstance, IDS_APP_TITLE, nid.szTip, MAX_LOADSTRING);
   Shell_NotifyIcon(NIM_ADD, &nid);
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_KEYDOWN:
		if (GetKeyState(VK_CONTROL) & GetKeyState(VK_SPACE))
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_STATISTIC), hWnd, StatisticDialog);
		}
		break;
	case WM_CREATE:
	{
		ReadFromFile();
		WriteToFile();
		HFONT hFont = CreateFont(24, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Castellar");
		HWND hGroupboxA = CreateWindowEx(0, L"BUTTON", NULL, WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP,
			20, 20, 540, 300, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxA, WM_SETFONT, WPARAM(hFont), TRUE);

		HWND hTemp = CreateWindowEx(0, L"STATIC", L"MY QUICKLAUNCH", WS_CHILD | WS_VISIBLE, 175, 20, 230, 23, hWnd, (HMENU)NULL, hInst, NULL);//200,10,180,23
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), true);

		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);

		// Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		hFont = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		g_hSearchBox = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 50, 80, 480, 25, hWnd, (HMENU)123, hInst, NULL);
		hTemp = CreateWindowEx(0, L"STATIC", L"Search for:", WS_CHILD | WS_VISIBLE, 50, 65, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		hTemp = CreateWindowEx(0, L"STATIC", L"Program:", WS_CHILD | WS_VISIBLE, 50, 125, 45, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		//Init listview properties
		long extStyle = WS_EX_CLIENTEDGE;
		long style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

		//Create ListView
		g_hListview = CreateWindowEx(extStyle, WC_LISTVIEW, _T("List View"),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | style,
			50, 140, 480, 150, hWnd, (HMENU)IDL_LISTVIEW, hInst, NULL);

		//Init 2 columns
		LVCOLUMN lvCol;
		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

		//Insert Key and Value column
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 232;
		lvCol.pszText = (LPWSTR)_T("Key");
		ListView_InsertColumn(g_hListview, 0, &lvCol);

		LVCOLUMN lvCol1;
		lvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

		lvCol1.fmt = LVCFMT_LEFT;
		lvCol1.cx = 248;
		lvCol1.pszText = (LPWSTR)_T("Value");
		ListView_InsertColumn(g_hListview, 1, &lvCol1);

		hTemp = CreateWindowEx(0, L"BUTTON", L"THOÁT", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			240, 340, 90, 35, hWnd, (HMENU)IDM_EXIT, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

	}
	break;
	case TRAY_ICON_NOTIFICATION:
		// systray msg callback 
		switch (LOWORD(lParam))
		{
		case WM_LBUTTONDBLCLK: // to open the GUI of the Application
			ShowWindow(hWnd, SW_NORMAL);
			return TRUE;
		case WM_RBUTTONDOWN:
			POINT  lpClickPoint;
			UINT uFlag = MF_BYPOSITION | MF_STRING;
			GetCursorPos(&lpClickPoint);
			hPopMenu = CreatePopupMenu();
			InsertMenu(hPopMenu, 0xFFFFFFFF, uFlag, IDC_TRAY_ICON_SCAN, _T("Scan to build database"));
			InsertMenu(hPopMenu, 0xFFFFFFFF, uFlag, IDC_BTN_STATISTIC, _T("View statistics (Ctrl + Space)"));
			InsertMenu(hPopMenu, 0xFFFFFFFF, MF_BYPOSITION | MF_STRING, IDM_EXIT, L"Exit");

			SetForegroundWindow(hWnd);
			TrackPopupMenu(hPopMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN, lpClickPoint.x, lpClickPoint.y, 0, hWnd, NULL);
			return TRUE;
		}
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		if ((HWND)lParam == g_hSearchBox) 
			if (wmEvent == EN_CHANGE) 
			{
				//triggered on keypress
				ScanProgram();
				GetWindowText(g_hSearchBox, keyword, 255);
				g_SearchList.clear();
				std::wstring key = keyword;
				std::transform(key.begin(), key.end(), key.begin(), ::tolower);
				for (int i = 0; i < g_List.size(); i++)
				{
					std::wstring temp = g_List[i].pr_Name;
					std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
					if (temp.find(key) == 0)
						g_SearchList.push_back(g_List[i]);
				}
				LoadToListView(g_SearchList);
				if (StrCmp(keyword, L"") == 0 && scan == false)
					ListView_DeleteAllItems(g_hListview);
			}
		switch (wmId)
		{
		case IDC_TRAY_ICON_SCAN:
		{
			ScanProgram();
			LoadToListView(g_List);
			scan = true;
			break;
		}
		case IDC_BTN_STATISTIC:
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_STATISTIC), hWnd, StatisticDialog);
			break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_NOTIFY:
	{
		NMHDR* notifyMess = (NMHDR*)lParam; //Notification Message
		switch (notifyMess->code)
		{
		case NM_DBLCLK:
			if (notifyMess->hwndFrom == g_hListview)
			{
				int i = ListView_GetSelectionMark(g_hListview);
				std::wstring exeString = g_SearchList[i].pr_Icon;
				if (!exeString.empty())
					exeString = exeString.substr(0, exeString.find(L"exe") + 3);
				else
					exeString = g_SearchList[i].pr_Location;

				//Increase frequency
				ShellExecute(NULL, _T("open"), exeString.c_str(), NULL, NULL, SW_SHOWNORMAL);
				g_List[g_SearchList[i].pr_index].pr_freq++;
				g_Sum += 1;
				WriteToFile();
				change = true;
			}
			break;
		case NM_RETURN:
			if (notifyMess->hwndFrom == g_hListview)
			{
				int i = ListView_GetSelectionMark(g_hListview);
				std::wstring exeString = g_SearchList[i].pr_Icon;
				if (!exeString.empty())
					exeString = exeString.substr(0, exeString.find(L"exe") + 3);
				else
					exeString = g_SearchList[i].pr_Location;

				//Increase frequency
				ShellExecute(NULL, _T("open"), exeString.c_str(), NULL, NULL, SW_SHOWNORMAL);
				g_List[g_SearchList[i].pr_index].pr_freq++;
				g_Sum += 1;
				WriteToFile();
				change = true;
			}
			break;
		}
	}
	break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
void deepSearch(TCHAR* appPath, int depthLevel)
{
	//Determine depth search level
	if (depthLevel >= 2)
		return;
	if (appPath == NULL || wcslen(appPath) == 0)
		return;

	TCHAR* content = new TCHAR[wcslen(appPath) + 2];
	StrCpy(content, appPath);
	StrCat(content, L"\\*");

	WIN32_FIND_DATA wfd;
	HANDLE hFile = FindFirstFileW(content, &wfd);
	bool isFound = true;
	if (hFile == INVALID_HANDLE_VALUE)
		isFound = false;

	while (isFound) 
	{
		if (depthLevel == 1)
			if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) //Get only directory and folder
			{
				isFound = FindNextFileW(hFile, &wfd);
				continue;
			}
		if ((StrCmp(wfd.cFileName, _T(".")) != 0) && (StrCmp(wfd.cFileName, _T("..")) != 0)) 
		{
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				//Recursion to dive to next depth level
				TCHAR* childDir;
				childDir = new TCHAR[wcslen(appPath) + wcslen(wfd.cFileName) + 2];
				StrCpy(childDir, appPath);
				StrCat(childDir, L"\\");
				StrCat(childDir, wfd.cFileName);
				deepSearch(childDir, depthLevel + 1);
				delete[] childDir;
			}
			else
				if (wcsstr(wfd.cFileName, L".exe") != nullptr)
				{
					TCHAR* childFile;
					childFile = new TCHAR[wcslen(appPath) + wcslen(wfd.cFileName) + 2];
					StrCpy(childFile, appPath);
					StrCat(childFile, L"\\");
					StrCat(childFile, wfd.cFileName);
					//Save data
					bool kt = false;
					for(int i=0;i<g_List.size();i++)
						if (StrCmp(wfd.cFileName, g_List[i].pr_Name.c_str()) == 0)
						{
							g_List[i].pr_Location = appPath;
							g_List[i].pr_Icon = childFile;
							kt = true;
							continue;
						}
					if (kt == false)
					{
						Program p;
						p.pr_Location = appPath;
						p.pr_Icon = childFile;
						p.pr_Name = wfd.cFileName;
						g_List.push_back(p);
					}
				}
		}
		isFound = FindNextFileW(hFile, &wfd);
	}
}
void ScanProgram()
{
	//Init find_data struct
	WIN32_FIND_DATA  wfd;
	TCHAR* Root = new TCHAR[wcslen(L"C:\\Program Files") + 2];
	StrCpy(Root, L"C:\\Program Files");
	StrCat(Root, _T("\\*"));

	HANDLE hFile = FindFirstFileW(Root, &wfd);
	bool found = true;
	if (hFile == INVALID_HANDLE_VALUE)
		found = false;

	while (found)
	{
		TCHAR* appPath;
		if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (StrCmp(wfd.cFileName, _T(".")) != 0) && (StrCmp(wfd.cFileName, _T("..")) != 0))
		{
			appPath = new TCHAR[wcslen(L"C:\\Program Files") + wcslen(wfd.cFileName) + 2];
			StrCpy(appPath, L"C:\\Program Files");
			StrCat(appPath, L"\\");
			StrCat(appPath, wfd.cFileName);

			deepSearch(appPath, 0);
		}
		found = FindNextFileW(hFile, &wfd);
	}
	for (int i = 0; i < g_List.size(); i++)
		for (int j = i + 1; j < g_List.size(); j++)
			if (g_List[i].pr_Name.compare(g_List[j].pr_Name) > 0)
			{
				Program temp = g_List[i];
				g_List[i] = g_List[j];
				g_List[j] = temp;
			}
	for (int i = 0; i < g_List.size(); i++)
		g_List[i].pr_index = i;
}
void LoadToListView(std::vector<Program> list)
{
	ListView_DeleteAllItems(g_hListview);
	for (int i = 0; i < list.size(); i++)
	{
		//Add to ListView
		LV_ITEM lv;
		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
		//Load colum 1
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = (LPWSTR)list[i].pr_Name.c_str();
		ListView_InsertItem(g_hListview, &lv);

		if (!list[i].pr_Location.empty())
		{
			lv.mask = LVIF_TEXT;

			//Load column 2
			lv.iSubItem = 1;
			lv.pszText = (LPWSTR)list[i].pr_Location.c_str();
			ListView_SetItem(g_hListview, &lv);
		}
	}
}
INT_PTR CALLBACK StatisticDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);	
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hDlg, &ps);
		// TODO: Add any drawing code that uses hdc here...
		if (change == true)
		{
			change = false;
			g_FreqNameList.clear();
			g_FreqNumbList.clear();
			ReadFromFile();
		}
		for (int i = 0; i < g_FreqNumbList.size(); i++)
			for (int j = i + 1; j < g_FreqNumbList.size(); j++)
				if (g_FreqNumbList[i] < g_FreqNumbList[j])
				{
					int temp = g_FreqNumbList[i];
					g_FreqNumbList[i] = g_FreqNumbList[j];
					g_FreqNumbList[j] = temp;
					std::wstring temp1 = g_FreqNameList[i];
					g_FreqNameList[i] = g_FreqNameList[j];
					g_FreqNameList[j] = temp1;
				}
		HFONT hFont_Title = CreateFont(30, 0, 0, 0, 1550, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, TEXT("Courier New"));
		HWND hWndStatic = CreateWindow(L"Static", L"TOP USAGE FREQUENCY APPS", WS_CHILD | WS_VISIBLE, 180, 30, 800, 30, hDlg, NULL, hInst, NULL);
		SendMessage(hWndStatic, WM_SETFONT, (WPARAM)hFont_Title, TRUE);

		HFONT hFont = CreateFont(16, 0, 0, 0, 550, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, TEXT("Courier New"));
		int yBar = 100;
		int realBarWidth = 0;
		int Red = 164, Green = 164, Blue = 210;
		HBRUSH hbrush = CreateSolidBrush(RGB(Red, Green, Blue));
		//begin draw chart
		std::wstringstream wss;
		wss.imbue(std::locale(""));
		int loop = 0;
		if (g_FreqNumbList.size() < 5)
			loop = g_FreqNumbList.size();
		else
			loop = 5;
		for (int i = 0; i < loop; i++)
		{
			float per = g_FreqNumbList[i] * 1.0 / g_Sum;
			realBarWidth = 350 * per + 3;
			RECT rect;
			rect.left = 360;
			rect.top = yBar;
			rect.right = 360 + realBarWidth;
			rect.bottom = yBar + 25;
			FillRect(hdc, &rect, hbrush);
			//write text
			wss.str(L"");//clear stream
			wss << g_FreqNameList[i] << "\0";
			HWND hWndName = CreateWindowEx(0, L"Static", wss.str().c_str(), WS_VISIBLE | WS_CHILD, 50, yBar, 300, 25, hDlg, NULL, NULL, NULL);
			wss.str(L"");
			wss << " (" << per * 100 << "%)\0";
			HWND hWndLast = CreateWindowEx(0, L"Static", wss.str().c_str(), WS_VISIBLE | WS_CHILD, 360 + realBarWidth, yBar, 300, 25, hDlg, NULL, NULL, NULL);
			SendMessage(hWndName, WM_SETFONT, (WPARAM)hFont, TRUE);
			SendMessage(hWndLast, WM_SETFONT, (WPARAM)hFont, TRUE);

			//generate new brush color for next bar
			Red -= 0;
			Green -= 25;
			Blue -= 10;
			hbrush = CreateSolidBrush(RGB(Red, Green, Blue));
			yBar += 50;
		}
		EndPaint(hDlg, &ps);
	}
	break;
	}
	return (INT_PTR)FALSE;
}
void WriteToFile()
{
	std::wofstream f(L"freq.txt");
	f << g_Sum << std::endl;
	for (int i = 0; i < g_List.size(); i++)
		if (g_List[i].pr_freq > 0)
		{
			f << g_List[i].pr_Name << std::endl;
			f << g_List[i].pr_freq << std::endl;
		}
	//Close file
	f.close();
}
void ReadFromFile()
{
	std::wfstream f;
	f.open(L"freq.txt", std::ios::in);
	std::wstring buffer;
	if (f.is_open())
	{
		if (getline(f, buffer))
			g_Sum = _wtoi64(buffer.c_str());
		//Get items
		while (getline(f, buffer))
		{
			Program p;
			g_FreqNameList.push_back(buffer);
			p.pr_Name = buffer;
			getline(f, buffer);
			g_FreqNumbList.push_back(_wtoi64(buffer.c_str()));
			p.pr_freq = _wtoi64(buffer.c_str());
			bool kt = false;
			for (int i = 0; i<g_List.size(); i++)
				if (StrCmp(p.pr_Name.c_str(), g_List[i].pr_Name.c_str()) == 0)
				{
					g_List[i].pr_Location = p.pr_freq;
					kt = true;
					continue;
				}
			if (kt == false)
				g_List.push_back(p);
		}
	}
	//Close file
	f.close();
}