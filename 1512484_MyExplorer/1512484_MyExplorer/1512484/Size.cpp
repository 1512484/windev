﻿#include "stdafx.h"
#include "Size.h"
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

CDriveSize::CDriveSize()
{
}

CDriveSize::CDriveSize(__int64 totalSize, __int64 freeSpace)
{
	mTotalSize = totalSize;
	mFreeSpace = freeSpace;
}



CDriveSize::~CDriveSize()
{
}


LPWSTR CDriveSize::convertByteToStringSize(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576)
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, 10);

	if (nRight != 0 && nType > 1)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, 10);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0:
		StrCat(buffer, _T(" bytes"));
		break;
	case 1:
		StrCat(buffer, _T(" KB"));
		break;
	case 2:
		StrCat(buffer, _T(" MB"));
		break;
	case 3:
		StrCat(buffer, _T(" GB"));
		break;
	case 4:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}

LPWSTR CDriveSize::getTotalSize()
{
	return convertByteToStringSize(mTotalSize);
}

LPWSTR CDriveSize::getFreeSpace()
{
	return convertByteToStringSize(mFreeSpace);
}
