// PaintDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "PaintDLL.h"

namespace PaintDLL 
{
	CLine::CLine()
	{
		x1 = 0;
		y1 = 0;
		x2 = 0;
		y2 = 0;
	}
	CLine::CLine(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void CLine::Draw(HDC hdc) 
	{
		MoveToEx(hdc, x1, y1, NULL);
		LineTo(hdc, x2, y2);
	}
	CRectangle::CRectangle()
	{
		x1 = 0;
		y1 = 0;
		x2 = 0;
		y2 = 0;
	}
	CRectangle::CRectangle(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void CRectangle::Draw(HDC hdc) 
	{
		Rectangle(hdc, x1, y1, x2, y2);
	}
	CEllipse::CEllipse()
	{
		x1 = 0;
		y1 = 0;
		x2 = 0;
		y2 = 0;
	}
	CEllipse::CEllipse(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void CEllipse::Draw(HDC hdc) 
	{
		Ellipse(hdc, x1, y1, x2, y2);
	}
	void myclass::SetShiftShape(int x1, int y1, int &x2, int &y2)
	{
		if (abs(x1 - x2) > abs(y1 - y2))
		{
			if (x1 > x2)
				x2 = x1 - abs(y1 - y2);
			else
				x2 = x1 + abs(y1 - y2);
		}
		else
		{
			if (y1 > y2)
				y2 = y1 - abs(x1 - x2);
			else
				y2 = y1 + abs(x1 - x2);
		}
	}
}

