#pragma once

#include <Windows.h>
#include <cmath>

#define PAINTLIBRARY_API __declspec(dllexport) 

namespace PaintDLL 
{
	class PAINTLIBRARY_API CShape 
	{
	public:
		virtual void Draw(HDC hdc) = 0;
	};


	class PAINTLIBRARY_API CLine : public CShape 
	{
	private:
		int x1;
		int y1;
		int x2;
		int y2;
	public:
		CLine();
		CLine(int a, int b, int c, int d);
		void Draw(HDC hdc);
	};

	class PAINTLIBRARY_API CRectangle : public CShape 
	{
	private:
		int x1;
		int y1;
		int x2;
		int y2;
	public:
		CRectangle();
		CRectangle(int a, int b, int c, int d);
		void Draw(HDC hdc);
	};
	class PAINTLIBRARY_API CEllipse : public CShape 
	{
	private:
		int x1;
		int y1;
		int x2;
		int y2;
	public:
		CEllipse();
		CEllipse(int a, int b, int c, int d);
		void Draw(HDC hdc);
	};
	class myclass
	{
	public:
		static PAINTLIBRARY_API void SetShiftShape(int x1, int y1, int &x2, int &y2);
	};
}
