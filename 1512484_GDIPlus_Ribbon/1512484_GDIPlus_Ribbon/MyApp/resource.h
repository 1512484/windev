//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyApp.rc
//
#define IDC_MYICON                      2
#define IDD_MYAPP_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYAPP                       107
#define IDI_SMALL                       108
#define IDC_MYAPP                       109
#define ID_LINE                         112
#define ID_RECTANGLE                    113
#define ID_ELLIPSE                      114
#define ID_SIZE_1                       115
#define ID_SIZE_2                       116
#define ID_SIZE_3                       117
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           118
#endif
#endif
