﻿// MyApp.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyApp.h"
#include <windowsX.h>
#include <vector>
#include <Objbase.h>
#include <objidl.h>
#include <gdiplus.h>
#pragma comment(lib, "Ole32.lib")
#pragma comment (lib,"Gdiplus.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"
#include "CommandHandler.h"
//#include "UIRibbonPropertyHelpers.h"
using namespace Gdiplus;
#define MAX_LOADSTRING 100
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	CoInitialize(NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYAPP, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYAPP));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	CoUninitialize();
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0; //CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYAPP);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"PAINT WITH GDIPLUS", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
int startX;
int startY;
int lastX;
int lastY;
bool isDrawing = false;
int LoaiHinh = 0;
int LoaiBut = 1;
typedef struct Hinh
{
	int left;
	int top;
	int width;
	int height;
	int Type;
	int TypePen;
};
std::vector<Hinh> shapes;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
STDMETHODIMP CCommandHandler::Execute(
	UINT nCmdID,
	UI_EXECUTIONVERB verb,
	const PROPERTYKEY* key,
	const PROPVARIANT* ppropvarValue,
	IUISimplePropertySet* pCommandExecutionProperties)
{
	UNREFERENCED_PARAMETER(pCommandExecutionProperties);
	UNREFERENCED_PARAMETER(ppropvarValue);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(verb);
	UNREFERENCED_PARAMETER(nCmdID);
	HRESULT hr;
	HWND hwnd = GetForegroundWindow();
	switch (nCmdID)
	{
	case ID_LINE:
		LoaiHinh = 0;
		break;
	case ID_RECTANGLE:
		LoaiHinh = 1;
		break;
	case ID_ELLIPSE:
		LoaiHinh = 2;
	case ID_SIZE_1:
		LoaiBut = 1;
		break;
	case ID_SIZE_2:
		LoaiBut = 2;
		break;
	case ID_SIZE_3:
		LoaiBut = 3;
		break;
	}
	return S_OK;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		startX = x;
		startY = y;
		isDrawing = true;
	}
	break;
	case WM_MOUSEMOVE: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		//WCHAR buffer[200];
		//wsprintf(buffer, L"%d, %d", x, y);
		//SetWindowText(hWnd, buffer);

		if (isDrawing) {
			lastX = x;
			lastY = y;
			InvalidateRect(hWnd, NULL, TRUE);
		}
	}
	break;
	case WM_LBUTTONUP: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		Hinh Temp;

		if (LoaiHinh == 0)
		{
			Temp.left = startX;
			Temp.top = startY;
			Temp.width = lastX;
			Temp.height = lastY;
			Temp.Type = 0;
		}
		if (LoaiHinh == 1)
		{
			Temp.Type = 1;
			if (GetKeyState(VK_SHIFT) & 0x8000)
			{
				if ((startX < lastX) && (startY < lastY))
				{
					Temp.left = startX;
					Temp.top = startY;
					Temp.width = lastX - startX;
					Temp.height = lastX - startX;
				}
				if ((startX > lastX) && (startY < lastY))
				{
					Temp.left = lastX;
					Temp.top = startY;
					Temp.width = startX - lastX;
					Temp.height = startX - lastX;
				}
				if ((startX > lastX) && (startY > lastY))
				{
					Temp.left = startX - startY + lastY;
					Temp.top = lastY;
					Temp.width = startY - lastY;
					Temp.height = startY - lastY;
				}
				if ((startX < lastX) && (startY > lastY))
				{
					Temp.left = startX;
					Temp.top = lastY;
					Temp.width = startY - lastY;
					Temp.height = startY - lastY;
				}
			}
			else
			{
				if ((startX < lastX) && (startY < lastY))
				{
					Temp.left = startX;
					Temp.top = startY;
					Temp.width = lastX - startX;
					Temp.height = lastY - startY;
				}
				if ((startX > lastX) && (startY < lastY))
				{
					Temp.left = lastX;
					Temp.top = startY;
					Temp.width = startX - lastX;
					Temp.height = lastY - startY;
				}
				if ((startX > lastX) && (startY > lastY))
				{
					Temp.left = lastX;
					Temp.top = lastY;
					Temp.width = startX - lastX;
					Temp.height = startY - lastY;
				}
				if ((startX < lastX) && (startY > lastY))
				{
					Temp.left = startX;
					Temp.top = lastY;
					Temp.width = lastX - startX;
					Temp.height = startY - lastY;
				}
			}
		}
		if (LoaiHinh == 2)
		{
			Temp.Type = 2;
			Temp.left = startX;
			Temp.top = startY;
			Temp.width = lastX - startX;
			if (GetKeyState(VK_SHIFT) & 0x8000)
			{
				if ((startX > lastX && startY < lastY) || (startX < lastX && startY > lastY))
					Temp.height = startX - lastX;
				if ((startX < lastX && startY < lastY) || (startX > lastX && startY > lastY))
					Temp.height = lastX - startX;
			}
			else
				Temp.height = lastY - startY;
		}
		if (LoaiBut == 1)
			Temp.TypePen = 1;
		if (LoaiBut == 2)
			Temp.TypePen = 2;
		if (LoaiBut == 3)
			Temp.TypePen = 3;
		shapes.push_back(Temp);
		isDrawing = false;
		InvalidateRect(hWnd, NULL, TRUE);
	}
	break;
	case WM_CREATE:
		InitializeFramework(hWnd);
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		for (int i = 0; i < shapes.size(); i++)
		{
			Pen* pen = new Pen(Color(255, 0, 0, 0), shapes[i].TypePen);
			Graphics* graphics = new Graphics(hdc);
			if (shapes[i].Type == 0)
				graphics->DrawLine(pen, shapes[i].left, shapes[i].top, shapes[i].width, shapes[i].height);
			if (shapes[i].Type == 1)
				graphics->DrawRectangle(pen, shapes[i].left, shapes[i].top, shapes[i].width, shapes[i].height);
			if (shapes[i].Type == 2)
				graphics->DrawEllipse(pen, shapes[i].left, shapes[i].top, shapes[i].width, shapes[i].height);
			delete pen;
			delete graphics;
		}
		if (isDrawing) {
			Pen* pen2 = new Pen(Color(255, 0, 0, 0), LoaiBut);
			Graphics* graphics2 = new Graphics(hdc);
			if (LoaiHinh == 0)
				graphics2->DrawLine(pen2, startX, startY, lastX, lastY);
			if (LoaiHinh == 1)
			{
				if (GetKeyState(VK_SHIFT) & 0x8000)
				{
					if ((startX < lastX) && (startY < lastY))
						graphics2->DrawRectangle(pen2, startX, startY, lastX - startX, lastX - startX);
					if ((startX > lastX) && (startY < lastY))
						graphics2->DrawRectangle(pen2, lastX, startY, startX - lastX, startX - lastX);
					if ((startX > lastX) && (startY > lastY))
						graphics2->DrawRectangle(pen2, startX - startY + lastY, lastY, startY - lastY, startY - lastY);
					if ((startX < lastX) && (startY > lastY))
						graphics2->DrawRectangle(pen2, startX, lastY, startY - lastY, startY - lastY);
				}
				else
				{
					if ((startX < lastX) && (startY < lastY))
						graphics2->DrawRectangle(pen2, startX, startY, lastX - startX, lastY - startY);
					if ((startX > lastX) && (startY < lastY))
						graphics2->DrawRectangle(pen2, lastX, startY, startX - lastX, lastY - startY);
					if ((startX > lastX) && (startY > lastY))
						graphics2->DrawRectangle(pen2, lastX, lastY, startX - lastX, startY - lastY);
					if ((startX < lastX) && (startY > lastY))
						graphics2->DrawRectangle(pen2, startX, lastY, lastX - startX, startY - lastY);
				}
			}
			if (LoaiHinh == 2)
			{
				if (GetKeyState(VK_SHIFT) & 0x8000)
				{
					if ((startX > lastX && startY < lastY) || (startX < lastX && startY > lastY))
						graphics2->DrawEllipse(pen2, startX, startY, lastX - startX, startX - lastX);

					if ((startX < lastX && startY < lastY) || (startX > lastX && startY > lastY))
						graphics2->DrawEllipse(pen2, startX, startY, lastX - startX, lastX - startX);
				}
				else
					graphics2->DrawEllipse(pen2, startX, startY, lastX - startX, lastY - startY);
			}
			delete pen2;
			delete graphics2;
		}
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		DestroyFramework();
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
