﻿#include "stdafx.h"
#include "Assistance.h"
#include <shellapi.h>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")


Assistance::Assistance()
{
	mDriveLetter = NULL;
	mVolumeLabel = NULL;
	mDriveType = NULL;
	mNumberOfDrive = 0;
}

Assistance::~Assistance()
{
	for (int i = 0; i < mNumberOfDrive; ++i)
	{
		delete[] mDriveLetter[i];
		delete[] mVolumeLabel[i];
		delete[] mDriveType[i];
	}

	delete[] mDriveLetter;
	delete[] mVolumeLabel;
	delete[] mDriveType;
	mNumberOfDrive = 0;
}


TCHAR* Assistance::getDriveType(int position)
{
	return mDriveType[position];
}

TCHAR* Assistance::getDriveLetter(const int &i)
{
	return mDriveLetter[i];
}


TCHAR* Assistance::getDisplayName(const int &i)
{
	return mVolumeLabel[i];
}

int Assistance::getCount()
{
	return mNumberOfDrive;
}


void Assistance::getSystemDrives()
{
	TCHAR buffer[105];
	TCHAR* volumeTempName = new TCHAR[105];
	GetLogicalDriveStrings(105, buffer);
	mNumberOfDrive = 0;
	for (int i = 0; (buffer[i] != '\0') || (buffer[i - 1] != '\0'); i++)
		if (buffer[i] == '\0')
			mNumberOfDrive++;
	mDriveLetter = new TCHAR*[mNumberOfDrive];
	mVolumeLabel = new TCHAR*[mNumberOfDrive];
	mDriveType = new TCHAR*[mNumberOfDrive];
	mDriveSize = new CDriveSize*[mNumberOfDrive];

	for (int i = 0; i < mNumberOfDrive; ++i)
	{
		mDriveLetter[i] = new TCHAR[4];
		mVolumeLabel[i] = new TCHAR[30];
		mDriveType[i] = new TCHAR[20];
	}
	int j, k;
	int index = 0;
	//Get drives'letter
	for (j = 0; j < mNumberOfDrive; ++j)
	{
		k = 0;
		while (buffer[index] != 0)
			mDriveLetter[j][k++] = buffer[index++];
		mDriveLetter[j][k] = '\0';
		++index;
	}

	//Get label name, type and size of every single volume
	int nType;
	for (int i = 0; i < mNumberOfDrive; ++i)
	{
		nType = GetDriveType(mDriveLetter[i]);
		switch (nType)
		{
		case DRIVE_FIXED:
			StrCpy(mDriveType[i], _T("Local Disk"));
			break;
		case DRIVE_REMOVABLE:
			StrCpy(mDriveType[i], _T("Removable Drive"));
			break;
		case DRIVE_REMOTE:
			StrCpy(mDriveType[i], _T("Network Drive"));
			break;
		case DRIVE_CDROM:
			StrCpy(mDriveType[i], _T("CD-ROM"));
			break;
		default:
			break;
		}

		//Get drive size
		mDriveSize[i] = getDriveSize(i);

		//Clear buffer
		StrCpy(buffer, _T(""));

		//If drive is Hard drives, USB flash drive, network drive
		if ((nType == DRIVE_FIXED) || ((nType == DRIVE_REMOVABLE)) || (nType == DRIVE_REMOTE))
		{
			GetVolumeInformation(mDriveLetter[i], buffer, 105, NULL, NULL, NULL, NULL, 0);
			StrCpy(volumeTempName, buffer);
		}
		else
			if (nType == DRIVE_CDROM)
			{
				GetVolumeInformation(mDriveLetter[i], buffer, 105, NULL, NULL, NULL, NULL, 0);

				if (wcslen(buffer) == 0)
				{
					StrCpy(volumeTempName, _T("CD-ROM"));
				}
				else
				{
					StrCpy(volumeTempName, buffer);
				}
			}
			else if (((i == 0) || (i == 1)) && (nType == DRIVE_REMOVABLE))
			{
				StrCpy(volumeTempName, _T("Talking to floppy disk is fail!!"));
			}
		if (wcslen(volumeTempName) == 0)
		{
			StrCpy(mVolumeLabel[i], _T("Local Disk"));
		}
		else
		{
			StrCpy(mVolumeLabel[i], volumeTempName);
		}
		StrCat(mVolumeLabel[i], _T(" ("));
		StrNCat(mVolumeLabel[i], mDriveLetter[i], 3);
		StrCat(mVolumeLabel[i], _T(")"));
	}
}

CDriveSize* Assistance::getDriveSize(int i)
{
	__int64 totalSize;
	__int64 freeSpace;

	SHGetDiskFreeSpaceEx(getDriveLetter(i), NULL, (PULARGE_INTEGER)&totalSize, (PULARGE_INTEGER)&freeSpace);

	CDriveSize* driveSize = new CDriveSize(totalSize, freeSpace);

	return driveSize;
}

LPWSTR Assistance::getFreeSpace(int i)
{
	return mDriveSize[i]->getFreeSpace();
}

LPWSTR Assistance::getTotalSize(int i)
{
	return mDriveSize[i]->getTotalSize();
}