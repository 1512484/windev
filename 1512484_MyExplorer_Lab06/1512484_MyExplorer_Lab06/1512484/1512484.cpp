﻿// 1512484.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512484.h"
#include <commctrl.h>
#include <shlwapi.h>
#include <shellapi.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
#pragma comment(lib, "shlwapi.lib")
#define MAX_LOADSTRING 100
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
RECT rcClient;                       // The parent window's client area.
HINSTANCE g_hInstance;
HWND	g_hWnd;
HWND	g_hTreeView;
HWND	g_hListView;
HWND	g_hStatus;
Assistance* g_Drive;
RECT g_TreeViewRect;
int thisPCIconIndex, driveIconIndex;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
HWND createListView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle);
HWND createTreeView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle);
void loadThisPCToTree(Assistance *drive, HWND m_hTreeView);
void loadThisPCToListView(Assistance *drive, HWND m_hListView);
void loadExpandedChild(HTREEITEM hCurrSelected, HWND m_hTreeView); //Load all child and child of child items in treeview
LPCWSTR getPath(HTREEITEM hItem, HWND m_hTreeView); //Get dir path of an item in Treeview
void loadTreeviewItem(HTREEITEM &hParent, LPCWSTR path, HWND m_hTreeView);
void loadListviewItem(LPCWSTR path, HWND m_hParent, HWND m_hListView, Assistance *drive);
void loadOrExecSelected(HWND m_hListView);
void loadDirItemToListview(HWND m_hParent, HWND m_hListView, LPCWSTR path);
void initListviewColumn(HWND m_hListView, int type);
LPWSTR GetDateModified(const FILETIME &ftLastWrite);
LPWSTR GetType(const WIN32_FIND_DATA &fd);
LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512484, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512484));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512484));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512484);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	static  BOOL        xSizing;
	const int BUFFERSIZE = 260;
	WCHAR buffer1[BUFFERSIZE];
	WCHAR buffer2[BUFFERSIZE];
	WCHAR curPath[BUFFERSIZE];
	WCHAR configPath[BUFFERSIZE];

	// Tạo đường dẫn tuyệt đối tới file config
	GetCurrentDirectory(BUFFERSIZE, curPath);
	wsprintf(configPath, L"%s\\config.ini", curPath);
    switch (message)
    {
	case WM_CREATE:
	{
		g_hWnd = hWnd;
		g_Drive = new Assistance();
		g_Drive->getSystemDrives();

		InitCommonControls();

		//Get main parent window size
		GetClientRect(hWnd, &rcClient);

		//Create treeview
		long extStyle = 0, style = TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS;
		g_hTreeView = createTreeView(extStyle, hWnd, IDT_TREEVIEW, g_hInstance, 0, 0, rcClient.right / 3, rcClient.bottom, style);
		loadThisPCToTree(g_Drive, g_hTreeView);
		SetFocus(g_hTreeView);

		//Create listview
		extStyle = WS_EX_CLIENTEDGE;
		style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

		g_hListView = createListView(extStyle, hWnd, IDL_LISTVIEW, g_hInstance, rcClient.right / 3, 0,
			(rcClient.right - rcClient.left) * 2 / 3 + 1, rcClient.bottom, style);
		loadThisPCToListView(g_Drive, g_hListView);
		g_hStatus = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP | SBARS_TOOLTIPS, 0, 0, CW_USEDEFAULT, 100,
			hWnd, (HMENU)IDC_STATUSBAR, hInst, NULL);
		int nStatusSize[] = { rcClient.right , -1 };
		SendMessage(g_hStatus, SB_SETPARTS, 3, (LPARAM)&nStatusSize);
		MoveWindow(g_hStatus, 0, 0, rcClient.right, rcClient.bottom, TRUE);

		TCHAR *buffer = new TCHAR[34];
		wsprintf(buffer, _T("This PC có tổng cộng %d ổ đĩa"), g_Drive->getCount());
	}
	break;
    case WM_COMMAND:
		RECT rect;
		int width;
		int height;
		int w, h;
		if (GetWindowRect(hWnd, &rect))
		{
			width = rect.right - rect.left;
			height = rect.bottom - rect.top;
		}
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_UPDATESIZE:
			TCHAR buf1[32];
			_itow_s(width, buf1, 10);
			TCHAR buf2[32];
			_itow_s(height, buf2, 10);
			WritePrivateProfileString(L"section", L"width", buf1, configPath);
			WritePrivateProfileString(L"section", L"height", buf2, configPath);
			MessageBox(0, L"Update successfully!", L"Success", MB_OK);
			break;
		case ID_FILE_LOADSIZE:
			GetPrivateProfileString(L"section", L"width", L"Default value", buffer1, BUFFERSIZE, configPath);
			GetPrivateProfileString(L"section", L"height", L"Default value", buffer2, BUFFERSIZE, configPath);
			w = _wtoi(buffer1);
			h = _wtoi(buffer2);
			MoveWindow(hWnd, rect.left, rect.top, w, h, TRUE);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_NOTIFY:
	{
		int nCurSelIndex;
		NMHDR* notifyMess = (NMHDR*)lParam;
		LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)notifyMess;
		HTREEITEM currSelected;

		switch (notifyMess->code)
		{
		case TVN_ITEMEXPANDING:
			currSelected = lpnmTree->itemNew.hItem;
			loadExpandedChild(currSelected, g_hTreeView);
			break;
		case TVN_SELCHANGED:
			//Retrieve currently selected item in TreeView
			currSelected = TreeView_GetSelection(g_hTreeView);
			TreeView_Expand(g_hTreeView, currSelected, TVE_EXPAND);
			//Clear ListView
			ListView_DeleteAllItems(g_hListView);
			loadListviewItem(getPath(currSelected, g_hTreeView), hWnd, g_hListView, g_Drive);
			break;

		case NM_CLICK:
			if (notifyMess->hwndFrom == g_hListView)
			{
				int nCurSelIndex = ListView_GetNextItem(GetDlgItem(hWnd, IDL_LISTVIEW), -1, LVNI_FOCUSED);
				TCHAR *text = new TCHAR[256];
				LVITEM lv;
				lv.mask = LVIF_TEXT;
				lv.iItem = nCurSelIndex;
				lv.iSubItem = 0;
				lv.pszText = text;
				lv.cchTextMax = 256;


				lv.iSubItem = 2;
				ListView_GetItem(g_hListView, &lv);

				if (!StrCmpI(lv.pszText, _T("Thư mục")))
					SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 0, NULL);
				else
				{
					lv.iSubItem = 3;
					ListView_GetItem(g_hListView, &lv);
					SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)text);
				}

			}

			break;

		case NM_DBLCLK:
			if (notifyMess->hwndFrom == g_hListView)
				loadOrExecSelected(g_hListView);
			break;
		case NM_CUSTOMDRAW: //Ve lai cua so con
			RECT newTreeRC;
			GetClientRect(g_hTreeView, &newTreeRC);

			if (newTreeRC.right != g_TreeViewRect.right)
			{
				RECT tree;
				GetWindowRect(GetDlgItem(hWnd, IDT_TREEVIEW), &tree);

				RECT main;
				GetWindowRect(hWnd, &main);

				MoveWindow(g_hListView, tree.right - tree.left, 0, main.right - tree.right, tree.bottom - tree.top, TRUE);
				ListView_Arrange(g_hListView, LVA_ALIGNTOP);
				g_TreeViewRect = newTreeRC;
			}
			break;
		}

	}
	break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_SIZE:

		RECT treeRC;
		GetWindowRect(g_hTreeView, &treeRC);
		MoveWindow(g_hTreeView, 0, 0, treeRC.right - treeRC.left, 443, SWP_SHOWWINDOW);
		GetWindowRect(g_hTreeView, &g_TreeViewRect);

		RECT tree;
		GetWindowRect(GetDlgItem(hWnd, IDT_TREEVIEW), &tree);

		RECT main;
		GetWindowRect(hWnd, &main);

		MoveWindow(g_hListView, tree.right - tree.left, 0, main.right - tree.right, tree.bottom - tree.top, TRUE);
		ListView_Arrange(g_hListView, LVA_ALIGNTOP);

		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
HWND createListView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle)
{
	HWND m_hListView = CreateWindowEx(lExtStyle, WC_LISTVIEW, _T("List View"),
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | lStyle,
		x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	LVCOLUMN lvCol, lvCol1, lvCol2, lvCol3;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 150;
	lvCol.pszText = _T("Name");
	ListView_InsertColumn(m_hListView, 0, &lvCol);

	lvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol1.fmt = LVCFMT_LEFT;
	lvCol1.pszText = _T("Type");
	lvCol1.cx = 125;
	ListView_InsertColumn(m_hListView, 1, &lvCol1);

	lvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol2.fmt = LVCFMT_LEFT;
	lvCol2.cx = 125;
	lvCol2.pszText = _T("Total Size");
	ListView_InsertColumn(m_hListView, 2, &lvCol2);

	lvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol3.fmt = LVCFMT_LEFT;
	lvCol3.pszText = _T("Free Space");
	lvCol3.cx = 125;
	ListView_InsertColumn(m_hListView, 3, &lvCol3);

	return m_hListView;
}

HWND createTreeView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle)
{
	HWND m_hTreeView = CreateWindowEx(lExtStyle, WC_TREEVIEW, _T("Tree View"),
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_SIZEBOX | WS_VSCROLL | WS_TABSTOP | lStyle,
		x, y, nWidth, nHeight, parentWnd,
		(HMENU)ID, hParentInst, NULL);
	return m_hTreeView;
}

void loadThisPCToTree(Assistance *drive, HWND m_hTreeView)
{
	TV_INSERTSTRUCT tvInsert;

	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	//Load My Computer
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.iImage = thisPCIconIndex;
	tvInsert.item.iSelectedImage = thisPCIconIndex;
	tvInsert.item.pszText = _T("This PC");
	tvInsert.item.lParam = (LPARAM)_T("ThisPC");
	HTREEITEM hThisPC = TreeView_InsertItem(m_hTreeView, &tvInsert);

	//Load volume
	for (int i = 0; i < g_Drive->getCount(); ++i)
	{
		tvInsert.hParent = hThisPC;
		tvInsert.item.iImage = driveIconIndex;
		tvInsert.item.iSelectedImage = driveIconIndex;
		tvInsert.item.pszText = g_Drive->getDisplayName(i);
		tvInsert.item.lParam = (LPARAM)g_Drive->getDriveLetter(i);
		HTREEITEM hDrive = TreeView_InsertItem(m_hTreeView, &tvInsert);

		loadTreeviewItem(hDrive, getPath(hDrive, m_hTreeView), m_hTreeView);
	}

	TreeView_Expand(m_hTreeView, hThisPC, TVE_EXPAND);
	TreeView_SelectItem(m_hTreeView, hThisPC);
}


void loadThisPCToListView(Assistance *drive, HWND m_hListView)
{
	//Init column of Listview
	initListviewColumn(m_hListView, 0);
	LV_ITEM lv;

	for (int i = 0; i < drive->getCount(); ++i)
	{
		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;

		//Load Label name and default icon to first column
		lv.iItem = i;
		lv.iImage = 0;
		lv.iSubItem = 0;
		lv.pszText = drive->getDisplayName(i);
		lv.lParam = (LPARAM)drive->getDriveLetter(i);
		ListView_InsertItem(m_hListView, &lv);

		//
		lv.mask = LVIF_TEXT;

		//Load Type of directory to second column
		lv.iSubItem = 1;
		lv.pszText = drive->getDriveType(i);
		ListView_SetItem(m_hListView, &lv);

		//Load total size to third column
		lv.iSubItem = 2;
		if (wcscmp(drive->getDriveType(i), _T("CD-ROM")) != 0)
			lv.pszText = drive->getTotalSize(i);
		else
			lv.pszText = NULL;
		ListView_SetItem(m_hListView, &lv);

		//Load Free Space to last column
		lv.iSubItem = 3;
		if (wcscmp(drive->getDriveType(i), _T("CD-ROM")) != 0)
			lv.pszText = drive->getFreeSpace(i);
		else
			lv.pszText = NULL;
		ListView_SetItem(m_hListView, &lv);
	}
}


LPCWSTR getPath(HTREEITEM hItem, HWND m_hTreeView)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(m_hTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

LPCWSTR getPath(HWND m_hListView, int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(m_hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

void loadExpandedChild(HTREEITEM hCurrSelected, HWND m_hTreeView)
{
	HTREEITEM thisPC = TreeView_GetRoot(m_hTreeView);
	if (hCurrSelected == thisPC)
		return;

	HTREEITEM hCurrSelectedChild = TreeView_GetChild(m_hTreeView, hCurrSelected);

	if (hCurrSelectedChild != NULL)
	{
		do
		{
			//Get child of this Current selected child
			if (TreeView_GetChild(m_hTreeView, hCurrSelectedChild) == NULL)
			{
				//Load all child of Current selected child	
				loadTreeviewItem(hCurrSelectedChild, getPath(hCurrSelectedChild, m_hTreeView), m_hTreeView);
			}
		} while (hCurrSelectedChild = TreeView_GetNextSibling(m_hTreeView, hCurrSelectedChild));
	}
	else
		loadTreeviewItem(hCurrSelected, getPath(hCurrSelected, m_hTreeView), m_hTreeView);
}

void loadTreeviewItem(HTREEITEM &hParent, LPCWSTR path, HWND m_hTreeView)
{
	TCHAR buffer[10240];
	StrCpy(buffer, path); //Copy the path of item
	StrCat(buffer, _T("\\*"));

	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_SORT;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.item.iImage = 0;
	tvInsert.item.iSelectedImage = 0;

	WIN32_FIND_DATA ffd;
	HANDLE hFind = FindFirstFileW(buffer, &ffd);

	if (hFind == INVALID_HANDLE_VALUE)
		return;

	TCHAR* folderPath;

	do
	{
		DWORD fileAttribute = ffd.dwFileAttributes;
		if ((fileAttribute & FILE_ATTRIBUTE_DIRECTORY)
			&& (fileAttribute != FILE_ATTRIBUTE_HIDDEN)
			&& (_tcscmp(ffd.cFileName, _T(".")) != 0) && (_tcscmp(ffd.cFileName, _T("..")) != 0))
		{
			//Set file name
			tvInsert.item.pszText = ffd.cFileName;
			folderPath = new TCHAR[wcslen(path) + wcslen(ffd.cFileName) + 2];

			//Set path
			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));
			StrCat(folderPath, ffd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;

			HTREEITEM hItem = TreeView_InsertItem(m_hTreeView, &tvInsert);
		}
	} while (FindNextFileW(hFind, &ffd));

}

void loadListviewItem(LPCWSTR path, HWND m_hParent, HWND m_hListView, Assistance *drive)
{
	if (path == NULL)
		return;

	LV_ITEM lv;
	if (_tcscmp(path, _T("ThisPC")) == 0)
	{
		initListviewColumn(m_hListView, 0);

		for (int i = 0; i < drive->getCount(); ++i)
		{
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = i;
			lv.iImage = 0;

			//Add label of drives or volume
			lv.iSubItem = 0;
			lv.pszText = drive->getDisplayName(i);
			lv.lParam = (LPARAM)drive->getDriveLetter(i);
			ListView_InsertItem(m_hListView, &lv);

			//Load (Type, Size, Free Space)
			lv.mask = LVIF_TEXT;

			//Load Drives's Type to second column
			lv.iSubItem = 1;
			lv.pszText = drive->getDriveType(i);
			ListView_SetItem(m_hListView, &lv);

			//Load size to third column
			lv.iSubItem = 2;
			if (wcscmp(drive->getDriveType(i), _T("CD-ROM")) != 0)
				lv.pszText = drive->getTotalSize(i);
			else
				lv.pszText = NULL;

			ListView_SetItem(m_hListView, &lv);

			//Load FreeSpace to last column
			lv.iSubItem = 3;
			if (wcscmp(drive->getDriveType(i), _T("CD-ROM")) != 0)
				lv.pszText = drive->getFreeSpace(i);
			else
				lv.pszText = NULL;

			ListView_SetItem(m_hListView, &lv);
		}
	}
	else
		loadDirItemToListview(g_hWnd, m_hListView, path);
}


void loadOrExecSelected(HWND m_hListView)
{
	LPCWSTR filePath = getPath(m_hListView, ListView_GetSelectionMark(m_hListView));

	WIN32_FIND_DATA fd;

	//Retrieves attributes for a specified file or directory.
	if (GetFileAttributesEx(filePath, GetFileExInfoStandard, &fd) != 0)
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			ListView_DeleteAllItems(m_hListView);
			loadDirItemToListview(g_hWnd, m_hListView, filePath);
		}
		else
			ShellExecute(NULL, _T("open"), filePath, NULL, NULL, SW_SHOWNORMAL);
}

void loadDirItemToListview(HWND m_hParent, HWND m_hListView, LPCWSTR path)
{
	initListviewColumn(m_hListView, 1);
	TCHAR buffer[10240];

	//Copy path to buffer
	StrCpy(buffer, path);

	if (wcslen(path) == 3)
		StrCat(buffer, _T("*"));
	else
		StrCat(buffer, _T("\\*"));

	WIN32_FIND_DATA fd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LV_ITEM lv;
	TCHAR* temporaryPath;
	int itemIndex = 0;


	//Find file and folder in this directory
	hFind = FindFirstFileW(buffer, &fd);
	if (hFind == INVALID_HANDLE_VALUE)
		return;
	do
	{
		//Get only folder
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(_tcscmp(fd.cFileName, _T(".")) != 0) && (_tcscmp(fd.cFileName, _T("..")) != 0)) //Ignore . (curr dir) and .. (parent dir)
		{
			//Get path of this folder
			temporaryPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(temporaryPath, path);

			if (wcslen(path) != 3)
				StrCat(temporaryPath, _T("\\"));

			StrCat(temporaryPath, fd.cFileName);

			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = itemIndex;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = 0;
			lv.lParam = (LPARAM)temporaryPath;
			ListView_InsertItem(m_hListView, &lv);

			//Second column is Date Modified
			ListView_SetItemText(m_hListView, itemIndex, 1, convertTimeStampToString(fd.ftLastWriteTime));
			ListView_SetItemText(m_hListView, itemIndex, 2, _T("File folder"));

			itemIndex++;
		}
	} while (FindNextFileW(hFind, &fd));

	DWORD fileSizeCount = 0;
	//Get all file in this directory
	hFind = FindFirstFileW(buffer, &fd);

	if (hFind == INVALID_HANDLE_VALUE)
		return;
	do
	{
		//Ignore all Directory and Folder
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			//Get file path
			temporaryPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(temporaryPath, path);

			if (wcslen(path) != 3)
				StrCat(temporaryPath, _T("\\"));

			StrCat(temporaryPath, fd.cFileName);

			//Add name and path to first column
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = itemIndex;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)temporaryPath;

			ListView_InsertItem(m_hListView, &lv);

			//Second column is Date Modified
			ListView_SetItemText(m_hListView, itemIndex, 1, convertTimeStampToString(fd.ftLastWriteTime));

			//Last column is Size
			DWORD fileSizeLow = fd.nFileSizeLow; //The low-order DWORD value of the file size, in bytes
			ListView_SetItemText(m_hListView, itemIndex, 3, CDriveSize::convertByteToStringSize(fileSizeLow));
			fileSizeCount += fd.nFileSizeLow;
			itemIndex++;
		}
	} while (FindNextFileW(hFind, &fd));

	DWORD fileSizeLow = fd.nFileSizeLow;
	SendMessage(GetDlgItem(m_hParent, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)CDriveSize::convertByteToStringSize(fileSizeCount));
}

void initListviewColumn(HWND m_hListView, int type)
{
	LVCOLUMN lvCol, lvCol1, lvCol2, lvCol3;
	if (type == 0)
	{
		lvCol.mask = LVCF_TEXT | LVCF_FMT;
		lvCol.fmt = LVCFMT_LEFT | LVCF_WIDTH;
		lvCol.cx = 100;
		lvCol.pszText = _T("Type");
		ListView_SetColumn(m_hListView, 1, &lvCol);

		lvCol1.mask = LVCF_TEXT | LVCF_FMT;
		lvCol1.fmt = LVCFMT_RIGHT | LVCF_WIDTH;
		lvCol1.cx = 80;
		lvCol1.pszText = _T("Total Size");
		ListView_SetColumn(m_hListView, 2, &lvCol1);

		lvCol2.mask = LVCF_TEXT | LVCF_FMT;
		lvCol2.fmt = LVCFMT_RIGHT | LVCF_WIDTH;
		lvCol2.cx = 80;
		lvCol2.pszText = _T("Free Space");
		ListView_SetColumn(m_hListView, 3, &lvCol2);
	}
	if (type == 1)
	{
		lvCol.mask = LVCF_WIDTH;
		lvCol.cx = 180;
		ListView_SetColumn(m_hListView, 0, &lvCol);

		lvCol1.mask = LVCF_TEXT | LVCF_FMT;
		lvCol1.fmt = LVCFMT_RIGHT;
		lvCol1.pszText = _T("Date Modified");
		ListView_SetColumn(m_hListView, 1, &lvCol1);

		lvCol2.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
		lvCol2.fmt = LVCFMT_LEFT;
		lvCol2.cx = 130;
		lvCol2.pszText = _T("Type");
		ListView_SetColumn(m_hListView, 2, &lvCol2);

		lvCol3.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
		lvCol3.fmt = LVCFMT_LEFT;
		lvCol3.cx = 130;
		lvCol3.pszText = _T("Size");
		ListView_SetColumn(m_hListView, 3, &lvCol3);
	}
}

LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite)
{
	TCHAR *buffer = new TCHAR[50];
	SYSTEMTIME st;

	char szLocalDate[255], szLocalTime[255];
	FileTimeToSystemTime(&ftLastWrite, &st);
	GetDateFormat(LOCALE_USER_DEFAULT, DATE_AUTOLAYOUT, &st, NULL,
		(LPWSTR)szLocalDate, 255);
	GetTimeFormat(LOCALE_USER_DEFAULT, 0, &st, NULL, (LPWSTR)szLocalTime, 255);

	//Concat to string
	wsprintf(buffer, L"%s %s", szLocalDate, szLocalTime);

	return buffer;
}
LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{
	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d %s"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour>12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour>12) ? (_T("PM")) : (_T("AM")));

	return buffer;
}

#define ENTIRE_STRING NULL

LPWSTR GetType(const WIN32_FIND_DATA &fd)
{
	int nDotPos = StrRStrI(fd.cFileName, ENTIRE_STRING, _T(".")) - fd.cFileName;
	int len = wcslen(fd.cFileName);

	if (nDotPos < 0 || nDotPos >= len) //Nếu không tìm thấy
		return _T("Unknown");

	TCHAR *szExtension = new TCHAR[len - nDotPos + 1];
	int i;

	for (i = nDotPos; i < len; ++i)
		szExtension[i - nDotPos] = fd.cFileName[i];
	szExtension[i - nDotPos] = NULL; //Kí tự kết thúc chuỗi

	if (!StrCmpI(szExtension, _T(".htm")) || !StrCmpI(szExtension, _T(".html")))
	{
		return _T("Web page");
	}
	TCHAR pszOut[256];
	HKEY hKey;
	DWORD dwType = REG_SZ;
	DWORD dwSize = 256;

	//Kiếm handle của extension tương ứng trong registry
	if (RegOpenKeyEx(HKEY_CLASSES_ROOT, szExtension, 0, KEY_READ, &hKey) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Unknown");
	}

	if (RegQueryValueEx(hKey, NULL, NULL, &dwType, (PBYTE)pszOut, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Unknown");
	}
	RegCloseKey(hKey);

	//Kiếm mô tả về thông tin của extension thông qua handle của key tương ứng trong registry
	TCHAR *pszPath = new TCHAR[1000];
	dwSize = 1000;
	if (RegOpenKeyEx(HKEY_CLASSES_ROOT, pszOut, 0, KEY_READ, &hKey) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Unknown");
	}

	if (RegQueryValueEx(hKey, NULL, NULL, &dwType, (PBYTE)pszPath, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return _T("Unknown");
	}
	RegCloseKey(hKey);

	return pszPath;
}