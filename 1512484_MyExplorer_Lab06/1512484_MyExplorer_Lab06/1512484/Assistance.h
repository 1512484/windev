#pragma once
#ifndef CDRIVE_H
#define CDRIVE_H

#include <windows.h>
#include <tchar.h>
#include "CDriveSize.h"

class Assistance
{
private:
	TCHAR** mDriveLetter;
	TCHAR** mVolumeLabel;
	TCHAR** mDriveType;
	int mNumberOfDrive;
	CDriveSize** mDriveSize;

	CDriveSize* getDriveSize(int i);

public:
	Assistance();
	~Assistance();

	TCHAR* getDriveLetter(const int &i);
	TCHAR* getDisplayName(const int &i);
	int getCount();


	void getSystemDrives();

	TCHAR* getDriveType(int position);
	LPWSTR getTotalSize(int i);
	LPWSTR getFreeSpace(int i);
};

#endif